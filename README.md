# Wayback::API

## Build status

[![pipeline status](https://gitlab.com/marghidanu/wayback-api/badges/master/pipeline.svg)](https://gitlab.com/marghidanu/wayback-api/commits/master)

## Development environment

```bash
vagrant up
vagrant ssh

sudo su -
cd /vagrant

perl Build.PL
./Build installdeps
./Build
./Build test

./Build && prove -b xt/
```

## Usage

### Perl API

```perl

use Wayback::API;

my $api = Wayback::API->new();

my $results = $api->search( 'archive.org',
	{
		filter => 'statuscode:200',
		matchType => 'prefix'
	}
);

foreach my $key ( keys( %{ $results } ) ) {
	my $latest = $results->{ $key }->[0];

	my $content = $api->fetch(
		$latest->{timestamp},
		$latest->{original},
	);
}
```

### CLI

```bash
wayback_downloader.pl -d ~/Downloads --from 2011 --to 2012 http://archive.org
```

### Wayback documentation

Additional documentation for the **Wayback Machine** can be found here:

* https://archive.org/help/wayback_api.php
* https://github.com/internetarchive/wayback/tree/master/wayback-cdx-server
