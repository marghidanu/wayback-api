package Wayback::API::Downloader {
	use MooseX::App::Simple;

	use Wayback::API;

	use Mojo::File qw( path );
	use Mojo::URL;

	parameter 'url' => (
		is => 'ro',
		isa => 'Str',
		required => 1,
	);

	option 'directory' => (
		is => 'ro',
		isa => 'Str',
		required => 1,
		cmd_aliases => [ qw( d ) ],
	);

	option 'from' => (
		is => 'ro',
		isa => 'Maybe[Str]',
		default => undef,
		cmd_aliases => [ qw( f ) ],
	);

	option 'to' => (
		is => 'ro',
		isa => 'Maybe[Str]',
		default => undef,
		cmd_aliases => [ qw( t ) ],
	);

	has '_api' => (
		is => 'ro',
		isa => 'Wayback::API',
		lazy => 1,
		default => sub {
			return Wayback::API->new();
		},
	);

	sub run {
		my $self = shift();

		my $args = {
			matchType => 'prefix',
			filter => 'statuscode:200'
		};

		$args->{from} = $self->from() if( defined( $self->from() ) );
		$args->{to} = $self->to() if( defined( $self->to() ) );

		my $page = 0;
		while( 1 ) {
			$args->{page} = $page++;

			my $results = $self->_api()->search( $self->url(), $args );
			last() unless( defined( $results ) );

			my $total = scalar( keys( %{ $results } ) );
			my $index = 0;

			printf( "Found %d entries on page: %d ...\n", $total, $page );
			foreach my $key ( sort( keys( %{ $results } ) ) ) {
				my ( $timestamp, $endpoint ) = map {
						$results->{ $key }->[0]->{ $_ }
				} qw( timestamp original );

				print( "Page: ${page} - (${index}/${total}) - Downloading ${timestamp} : ${endpoint}\n" );

				# TODO: This needs some refactoring ...
				my $url = Mojo::URL->new( $endpoint );
				my $parts = $url->path()->parts();
				my $base = path( $self->directory() )->child( $url->host(), @{ $parts } );
				$base = $base->child( 'index.html' )
					if( $url->path()->trailing_slash() || !scalar( @{ $parts } ) );

				my $content = eval { $self->_api()->fetch( $timestamp, $endpoint ) };
				warn( "Error: $@" ) && next()
					if( $@ );

				$base->dirname()->make_path();
				$base->spurt( $content );

				$index++;
			}
		}

		return;
	}
}

1;

__END__

=head1 NAME

Wayback::API::Downloader

=head1 DESCRIPTION

=head1 METHODS

=head2 run

=head1 AUTHOR

Tudor Marghidanu <tudor@marghidanu.com>
