package Wayback::API {
	use Moose;

	use Mojo::UserAgent;
	use Mojo::URL;

	use List::MoreUtils qw( mesh );

	our $VERSION = '0.0.0';

	has '_ua' => (
		is => 'ro',
		isa => 'Mojo::UserAgent',
		default => sub {
			return Mojo::UserAgent->new(
				max_redirects => 10,
			),
		}
	);

	sub _cdx {
		my ( $self, $url, $args ) = @_;

		$args ||= {};
		$args->{url} = $url;
		$args->{output} = 'json';

		my $address = Mojo::URL->new( 'http://web.archive.org/cdx/search/cdx' );
		$address->query( $args );

		my $result = $self->_ua()->get( $address )->result();
		die( $result->message() )
			if( $result->is_error() );

		return ( $result->body_size() ) ? $result->json() : undef;
	}

	sub search {
		my ( $self, @args ) = @_;

		my $result = $self->_cdx( @args );
		return
			unless( defined( $result ) );

		my @head = @{ shift( @{ $result } ) };

		my %results = ();
		push( @{ $results{ $_->{urlkey} } }, $_ )
			foreach ( map { { mesh( @head, @{ $_ } ) } } @{ $result } );

		$results{ $_ } = [ sort { $a->{timestamp} > $b->{timestamp} } @{ $results{ $_ } } ]
			foreach ( keys( %results ) );

		return \%results;
	}

	sub fetch {
		my ( $self, $timestamp, $endpoint ) = @_;

		# TODO: This needs to look nicer ...
		my $url = Mojo::URL->new( 'http://web.archive.org' )
			->path( sprintf( '/web/%sid_/%s', $timestamp, $endpoint ) );

		# TODO: Add error management ...
		my $result = $self->_ua()->get( $url )
			->result();

		return $result->body();
	}

	__PACKAGE__->meta()->make_immutable();
}

1;

__END__

=head1 NAME

Wayback::API

=head1 DESCRIPTION

=head1 METHODS

=head2 fetch

=head2 search

=head1 AUTHOR

Tudor Marghidanu <tudor@marghidanu.com>
