#!/usr/bin/env perl

use strict;
use warnings;

use Test::More;

require_ok( 'Wayback::API' );

my $api = Wayback::API->new();
isa_ok( $api, 'Wayback::API' );
can_ok( $api, qw( search fetch ) );

{
	my $result = $api->search( 'oimr.stuffo.info', { from => 2015 } );

	ok( scalar( keys( %{ $result } ) ), 'Has results' );
	ok( scalar( @{ $result->{'info,stuffo,oimr)/'} } ), 'Has timestamps' )
}

done_testing();
