#!/usr/bin/env perl

use Wayback::API::Downloader;

Wayback::API::Downloader->new_with_options()
	->run();
